
	Pearson's product-moment correlation

data:  bowl18_triad_comparison$our_data and bowl18_triad_comparison$bowl_18_rating
t = 7.6218, df = 52, p-value = 5.06e-10
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.5693972 0.8322861
sample estimates:
      cor 
0.7264081 

