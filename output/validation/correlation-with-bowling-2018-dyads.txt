$pearson

	Pearson's product-moment correlation

data:  bowl18_comparison$our_mean and bowl18_comparison$bowl18_attractiveness
t = 6.9849, df = 10, p-value = 3.784e-05
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.7064101 0.9750937
sample estimates:
      cor 
0.9109881 


$spearman

	Spearman's rank correlation rho

data:  bowl18_comparison$our_mean and bowl18_comparison$bowl18_attractiveness
S = 14.525, p-value = 2.443e-06
alternative hypothesis: true rho is not equal to 0
sample estimates:
      rho 
0.9492134 


