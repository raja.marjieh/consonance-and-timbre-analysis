$pearson

	Pearson's product-moment correlation

data:  df_previous_studies$our_mean and df_previous_studies$mcpherson_mean
t = 10.345, df = 13, p-value = 1.213e-07
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.8368054 0.9816855
sample estimates:
      cor 
0.9442864 


$spearman

	Spearman's rank correlation rho

data:  df_previous_studies$our_mean and df_previous_studies$mcpherson_mean
S = 32, p-value < 2.2e-16
alternative hypothesis: true rho is not equal to 0
sample estimates:
      rho 
0.9428571 


